# -*- coding: utf-8 -*-

import urllib2
import urllib
import re
import time
import datetime
import os
from bs4 import BeautifulSoup

URL = "http://www.disneyclips.com/imagesnewb/lilostitch.html"

def mkdir(dirname):         #function for making a directory
    try:
        os.mkdir(dirname)
    except Exeption as e:
        print(e)

def get_img_url(x,url):       #function for changing the source to the URL
    if "http" not in x:
        return url+x
    else:
        return x

def dl_images(image_items):  #function for Downloading the images
    success_num = 0                     #successed downloading(success_num) images
    for img in image_items:
        source = img.get("src")         #from img get src
        imgURL = get_img_url(source, url)
        fname = source.split("/")[-1]   #image/stitch10.png to stitch.png
        
        try:
            if fname in os.listdir(dirname):
                fname = fname + str(success_num)
            urllib.urlretrieve(imgURL, dirname+"/"+fname)   #download!!
            print "[ Success ]" + imgURL
            success_num += 1
        except Exception, e:
            print e, "error occured for" + imgURL

    return success_num                  #return the number of successful downloads



if __name__ == '__main__':

    now = datetime.datetime.today()
    dirname = '{}{:0>2}{:0>2}{:0>2}{:0>2}{:0>6}'\
        .format(now.year, now.month, now.day,
                now.minute, now.second, now.microsecond)
    mkdir(dirname)                      #make a directory, named by date
    
    html = urllib2.urlopen(URL)         #take the html from URL
    url = URL.split("lilo")[0]          #http://www.disneyclips.com/imagesnewb/

    soup = BeautifulSoup(html, "html.parser")
    images_part = soup.find("div", {"id": "images"})    #find the part of stitch images
    image_items = images_part.find_all('img')           #from it, find all the img tag
    success_num = dl_images(image_items)                 #download!
    img_num = len(image_items)                          #get the number of images
    print success_num, "images could be downloaded (in", img_num, " images)."
