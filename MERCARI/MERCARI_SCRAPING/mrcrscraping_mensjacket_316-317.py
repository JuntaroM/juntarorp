# -*- coding: utf-8 -*-

import time
import os
import datetime
from datetime import datetime
import urllib.request
import urllib
from bs4 import BeautifulSoup
import pymysql.cursors
from slackweb import slackweb


# Parameter Declaration
MERCARI_URL = "https://www.mercari.com/jp/"
CATEGORY_TO_SCRAPE_START_NUM = 316
CATEGORY_TO_SCRAPE_END_NUM = 316
CATEGORY_NAME = "MENS' JACKETS No.316"
TIME_SLEEP = 1
TIME_SLEEP_MYSQL = 0.1
UNSOLD_SURPLUS = 10
IMAGES_SAVING_DIRECTORY = "/mnt/drobo/matsumoto/MERCARI_IMAGES"
TIME_LIMIT_HOURS = 18
CICLE_END_NUM = 10000

# connect to MySQL
connector = pymysql.connect(host = '133.11.86.142', user = 'matsumoto', passwd = 'ud0nud0n', db = 'matsumoto', charset="utf8")
cursor = connector.cursor()

# getting the last page
def getting_the_last_page(first_page_url):
    
    html = urllib.request.urlopen(first_page_url)
    soup = BeautifulSoup(html, "html.parser")
    div_part_to_find = soup.find("ul", {"class":"pager"})
    the_a_tag_to_find = div_part_to_find.find(lambda tag : tag.name == 'a' and tag.find('i') != None and 'icon-arrow-double-right' in tag.find('i').get('class'))
    the_href_to_find = the_a_tag_to_find.get("href")
    page_number_str = the_href_to_find.split("/?page=")[1]
    page_number_int = int(page_number_str)
    
    return page_number_int

# error report to slack
def slack_inform(message):
    slack = slackweb.Slack(url = "https://hooks.slack.com/services/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")
    slack.notify(text = message)

# url to html
def urlopen(url,statement):
    try:
        html = urllib.request.urlopen(url)
        soup = BeautifulSoup(html, "html.parser")
        time.sleep(TIME_SLEEP)
        return soup
        
    except:
        print ("[ ERROR ] for "+CATEGORY_NAME+": Error occured at openning the "+ statement + ": " + url)
        slack_inform("[ ERROR ] for "+CATEGORY_NAME+": Error occured at openning the " + statement + ": " + url)

# getting descriptions of product
def getting_description_from_product_page(div_part_to_find,keyword,html,english_name):
    description_part = div_part_to_find.find(lambda tag : tag.name == "tr" and tag.find("th") != None and keyword in tag.find("th").string)
    
    if description_part != None:
        description_name_part = description_part.find(html)
        if description_name_part != None:
            description = description_name_part.string.replace(' ','')
        else:
            description = "no "+english_name
    else:
        description = "no "+english_name
    return description

# making the MERCARI_DATA2 for each product (already sold out)
def MERCARI_DATA2_maker(product_url):
    
    soup = urlopen(product_url,"PRODUCT URL")
    
    if soup.find("h2", {"class":"deleted-item-name"}) != None:
        return "DELETED ITEM"
    else:
        div_part_to_find = soup.find("section", {"class":"item-box-container"})
        
        # getting the image urls
        images_div = div_part_to_find.find("div", {"class":"owl-carousel"})
        image_divs = images_div.find_all("img", {"class":"owl-lazy"})
        image_url = ""
        for div in image_divs:
            if image_url=="":
                image_url = div['data-src']
            else:
                image_url = image_url+", "+div['data-src']
        
        # getting the seller's information - good,normal, & bad ratings
        rating_user_div = div_part_to_find.find_all("div", {"class":"item-user-ratings"})
        good_rating = int(rating_user_div[0].span.string)
        normal_rating = int(rating_user_div[1].span.string)
        bad_rating = int(rating_user_div[2].span.string)

        # getting the seller's information - user's ID, number of products
        user_url_part1 = div_part_to_find.find("table", {"class":"item-detail-table"})
        user_url_part2 = user_url_part1.find(lambda tag : tag.name == 'a' and 'https://www.mercari.com/jp/u' in tag.get('href'))
        user_url = user_url_part2['href']
        user_id = user_url.split("/")[-2]
        
        soup_user = urlopen(user_url,"USER'S URL")
        
        div_part_to_find_user = soup_user.find("div", {"class":"users-detail-score font-3"})
        number_of_products_part = div_part_to_find_user.find_all("span")[1].string
        number_of_products = int(number_of_products_part.split(' ')[1])
    
        # getting the category
        category_part = div_part_to_find.find(lambda tag : tag.name == "tr" and tag.find("th") != None and "カテゴリー" in tag.find("th").string)
        a_tag_category = category_part.find_all("a")
        if len(a_tag_category)==3:
            category_1 = str("category-" + a_tag_category[0]["href"].split("category/")[1].replace("/",""))
            category_2 = str("category-" + a_tag_category[1]["href"].split("category/")[1].replace("/",""))
            category_3 = str("category-" + a_tag_category[2]["href"].split("category/")[1].replace("/",""))
        elif len(a_tag_category)==2:
            category_1 = str("category-" + a_tag_category[0]["href"].split("category/")[1].replace("/",""))
            category_2 = str("category-" + a_tag_category[1]["href"].split("category/")[1].replace("/",""))
            category_3 = "no category"
        elif len(a_tag_category)==1:
            category_1 = str("category-" + a_tag_category[0]["href"].split("category/")[1].replace("/",""))
            category_2 = "no category"
            category_3 = "no category"
            
        # getting the brand
        brand_name = getting_description_from_product_page(div_part_to_find,"ブランド","div","brand")
    
        # getting the size
        product_size = getting_description_from_product_page(div_part_to_find,"商品のサイズ","td","size")

        # getting the condition
        product_condition = getting_description_from_product_page(div_part_to_find,"商品の状態","td","description")
    
        # getting the person to pay the delivery charge
        delivery_charger = getting_description_from_product_page(div_part_to_find,"配送料の負担","td","description")

        # getting the person to pay the shipping method
        shipping_method = getting_description_from_product_page(div_part_to_find,"配送の方法","td","description")
    
        # getting seller's address
        seller_address = getting_description_from_product_page(div_part_to_find,"配送元地域","td","description")
        
        # getting the time it takes to start shipping
        time_to_ship = getting_description_from_product_page(div_part_to_find,"発送日の目安","td","description")

        # getting the description
        description_part = div_part_to_find.find("div", {"class":"item-description f14"})
        description = str(description_part).replace('<div class="item-description f14">','').replace('<br/>','').replace('</div>','')
    
        # getting the number of Like! button clicked
        like_button_part = div_part_to_find.find("span", {"data-num":"like"})
        like_button_number = int(like_button_part.string)
    
        # getting the item name
        item_name_part = div_part_to_find.find("h2", {"class":"item-name"})
        if item_name_part != None:
            item_name = item_name_part.string
        else:
            item_name = "none"
    
        DATA = [category_1, category_2, category_3, image_url, user_id, good_rating, normal_rating, bad_rating, number_of_products, product_size, product_condition, delivery_charger, shipping_method, seller_address, time_to_ship, like_button_number, item_name, brand_name, description]
        return DATA

# making the MERCARI_DATA1 for each product from html
def MERCARI_DATA1_maker(product_html_part):
    # getting the URL
    the_a_tag_to_find = product_html_part.find('a')
    the_href_to_find = the_a_tag_to_find.get("href")
    # getting the id
    id_to_find_pre = the_href_to_find.split(".com/jp/")[1]
    id_to_find = id_to_find_pre.split("/")[0]
    # getting the date and time
    date_and_time = time.strftime('%Y-%m-%d %H:%M:%S')
    # getting sold_or_not
    sold_division = product_html_part.find("div", {"class":"item-sold-out-badge"})
    if sold_division == None:
        sold_or_not = "NO"
    else:
        sold_or_not = "YES"
    # getting the cost
    cost_get = product_html_part.find("div", {"class":"items-box-price font-5"})
    cost_get_str = cost_get.string
    cost_get_int = int(cost_get_str.split(" ")[1].replace(',',''))
            
    DATA = [id_to_find,date_and_time,the_href_to_find,sold_or_not,cost_get_int]
    return DATA

# finding out the time it took to get sold
def time_to_get_sold_getter(id):
    try:
        statement = """SELECT date FROM MERCARI_DATA1 WHERE id like '%s' limit 1"""
        statement = statement % ('%%%s%%' % id)
        cursor.execute(statement)
        time.sleep(TIME_SLEEP_MYSQL)
        records = cursor.fetchone()
        if records != None:
            time_to_get_sold = (datetime.now()-records[0]).seconds
            added_timing = "SECANDARY"
        else:
            time_to_get_sold = 0
            added_timing = "FIRST ADDED"
        return [time_to_get_sold,added_timing]
    
    except:
        print ("[ ERROR ] for "+CATEGORY_NAME+": Error occured at getting the time_to_get_sold for ID: " + id)
        slack_inform("[ ERROR ] for "+CATEGORY_NAME+": Error occured at getting the time_to_get_sold for ID: " + id)
    
# makingthe MERCARI_DATA1 for each product
def main(product_list_url,page_num):
    
    soup = urlopen(product_list_url,"PRODUCT LIST URL")
    
    div_part_to_find = soup.find("div", {"class":"items-box-content clearfix"})
    section_part_htmllist = div_part_to_find.find_all("section", {"class":"items-box"})
    
    # getting information from each item section
    for section in section_part_htmllist:
        # getting MERCARI_DATA1
        try:
            data1 = MERCARI_DATA1_maker(section)
            
        except:
            print ("[ ERROR ] for "+CATEGORY_NAME+": Error occured at getting data1")
            slack_inform("[ ERROR ] for "+CATEGORY_NAME+": Error occured at getting data1")
        
        time_it_took_to_be_sold = time_to_get_sold_getter(data1[0])
        data1.append(time_it_took_to_be_sold[1])
        
        try:
            if time_it_took_to_be_sold[1] == "FIRST ADDED":
                statement_data1 = "INSERT INTO MERCARI_DATA1 VALUES (%s,%s,%s,%s,%s,%s)"
                cursor.execute(statement_data1,data1)
                time.sleep(TIME_SLEEP_MYSQL)
            else:
                statement_data1 = "REPLACE INTO MERCARI_DATA1 VALUES (%s,%s,%s,%s,%s,%s)"
                cursor.execute(statement_data1,data1)
                time.sleep(TIME_SLEEP_MYSQL)
            
            print ("[ SUCCESS ] for data instalation in MERCARI_DATA1 for ID: " + data1[0])
        except:
            print ("[ ERROR ] for "+CATEGORY_NAME+": Error occured at inserting to MERCARI_DATA1 for ID: " + data1[0])
            slack_inform("[ ERROR ] for "+CATEGORY_NAME+": Error occured at inserting to MERCARI_DATA1 for ID: " + data1[0])
        
        id_to_integer = int(data1[0].replace("m",""))
        
        if data1[3] == "YES" or (time_it_took_to_be_sold[0] >= TIME_LIMIT_HOURS*60*60 and id_to_integer%UNSOLD_SURPLUS==0):
            statement_to_find_id = """SELECT date FROM MERCARI_DATA2 where id like '%s'"""
            statement_to_find_id = statement_to_find_id % ('%%%s%%' % data1[0])
            id_exist_or_not = cursor.execute(statement_to_find_id)
            time.sleep(TIME_SLEEP_MYSQL)
            if id_exist_or_not == 0:
                try:
                    data2 = MERCARI_DATA2_maker(data1[2])
                except:
                    print ("[ ERROR ] for "+CATEGORY_NAME+": Error occured at getting data2 for ID: " + data1[0])
                    slack_inform("[ ERROR ] for "+CATEGORY_NAME+": Error occured at getting data2 for ID: " + data1[0])

                if data2 == "DELETED ITEM":
                    print("[ EXCEPTION ] for "+CATEGORY_NAME+": Item already deleted for ID: " + data1[0])

                else:
                    list_to_add = [data1[0],data1[1],time_it_took_to_be_sold[0],data1[3],page_num,data1[4]]
                    try:
                        statement_data2 = "INSERT INTO MERCARI_DATA2 VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                        cursor.execute(statement_data2,list_to_add+data2)
                        time.sleep(TIME_SLEEP_MYSQL)
                        
                        print ("[ SUCCESS ] for data instalation in MERCARI_DATA2 for ID: " + data1[0])
                    except:
                        print ("[ ERROR ] for "+CATEGORY_NAME+": Error occured at inserting to MERCARI_DATA2 for ID: " + data1[0])
                        slack_inform("[ ERROR ] for "+CATEGORY_NAME+": Error occured at inserting to MERCARI_DATA2 for ID: " + data1[0])
                
                    try:
                        img_urls = data2[3].split(", ")
                        count = 1
                        for img_url in img_urls:
                            fname = data1[0] + "_" + str(count) + ".jpg"
                            if fname in os.listdir(IMAGES_SAVING_DIRECTORY):
                                fname = data1[0] + "_" + str(count+1) + ".jpg"
                                count += 1
                            urllib.request.urlretrieve(img_url, IMAGES_SAVING_DIRECTORY + "/" + fname)
                            print ("[ SUCCESS ] for image downloading " + fname)
                    except:
                        print("[ ERROR ] for "+CATEGORY_NAME+": image download error for ID: " + data1[0])
                        slack_inform("[ ERROR ] for "+CATEGORY_NAME+": image download error for ID: " + data1[0])
                    
            elif id_exist_or_not == 1:
                print("[ EXCEPTION ] Data for MERCARI_DATA2 already exists. ")
            else:
                print("[ ERROR ] for "+CATEGORY_NAME+": Error occured at inserting MERCARI_DATA2: id_exists_or_not error for ID: "+data1[0])
                slack_inform("[ ERROR ] for "+CATEGORY_NAME+": Error occured at inserting MERCARI_DATA2: id_exists_or_not error for ID: "+data1[0])


# main: making the data1 and data2 for soldout items
if __name__ == '__main__':

    # cicle starts!!
    for CICLE in range(1,CICLE_END_NUM+1):

        print("[ NOTIFICATION ] for "+CATEGORY_NAME+": cicle number "+str(CICLE)+" have started.")
        #slack_inform("[ NOTIFICATION ] for "+CATEGORY_NAME+": cicle number "+str(CICLE)+" have started.")

        # for each sub-categories
        for category_num in range(CATEGORY_TO_SCRAPE_START_NUM,CATEGORY_TO_SCRAPE_END_NUM+1):
            # getting the last page
            try:
                last_page = getting_the_last_page(MERCARI_URL+"category/"+str(category_num)+"/")
                time.sleep(TIME_SLEEP)
            except:
                print("[ ERROR ] for "+CATEGORY_NAME+": Error occured at getting the last page for category number: "+str(category_num))
                slack_inform("[ ERROR ] for "+CATEGORY_NAME+": Error occured at getting the last page for category number: "+str(category_num))

            # for each product list page in the sub-category
            for page_num in range(1,last_page+1):
                
                try:
                    main(MERCARI_URL+"category/"+str(category_num)+"/?page="+str(page_num),page_num)
                    connector.commit()
                    time.sleep(TIME_SLEEP_MYSQL)
                except:
                    print("[ ERROR ] for "+CATEGORY_NAME+": Error occured at moving main() for category number "+str(category_num)+"'s page "+str(page_num))
                    slack_inform("[ ERROR ] for "+CATEGORY_NAME+": Error occured at moving main() for category number "+str(category_num)+"'s page "+str(page_num))


        print("[ NOTIFICATION ] for "+CATEGORY_NAME+": "+str(CICLE)+" cicles have been done for category "+CATEGORY_NAME+".")
        

    print("[ NOTIFICATION ] for "+CATEGORY_NAME+": EVERYTHING DONE!!")
    slack_inform("[ NOTIFICATION ] for "+CATEGORY_NAME+": EVERYTHING DONE!!")
