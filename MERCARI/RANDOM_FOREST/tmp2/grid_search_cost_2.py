# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np
import pickle
from sklearn.ensemble import RandomForestClassifier
from sklearn import cross_validation
from time import time
from sklearn.model_selection import GridSearchCV

NUM = 2
NUM_NUM = 1500

print("Loading pickle files")
label_and_vector = pickle.load(open("/mnt/drobo/matsumoto/PICKLE_FILE_cost/label_and_vector_cost"+str(NUM)+".p","rb"))
id_list = pickle.load(open("/mnt/drobo/matsumoto/PICKLE_FILE_cost/id_list_cost"+str(NUM)+".p","rb"))

# ---------------label_and_vectro to all_answer and all_array---------------
all_array = []
all_answer = []
for id_ in id_list:
    label = label_and_vector[id_+"_label"]
    vector = label_and_vector[id_+"_vector"]
    if label != "SECOND CLASS":
        all_array.append(vector)
        all_answer.append(label)
        
# ---------------decrese float's numerical memory-cosistence---------------not used now
all_vector = []
for vector in all_array:
    all_vector_pre = []
    for float_1 in vector:
        float_2 = int(float_1*100000000)
        float_3 = float_2/100000000
        all_vector_pre.append(float_3)
    all_vector.append(all_vector_pre)
all_array = []
all_array = all_vector

# ---------------change all_answer to {0,1}---------------
tmp = []
for i in all_answer:
    if i == 'FIRST CLASS':
        tmp.append(0)
    if i == 'THIRD CLASS':
        tmp.append(1)
all_answer = []
all_answer = tmp

all_array_ = []
for i in range(len(all_array)):
    array = all_array[i][0:39]+all_array[i][63:2081]
    all_array_.append(array)
all_array = []
all_array = all_array_

all_array_df = pd.DataFrame(all_array)
all_answer_df = pd.DataFrame(all_answer)
all_answer_df.columns = ["ans"]
all_data_df = pd.concat([all_array_df, all_answer_df], axis=1)

ans_df_1 = all_data_df[all_data_df.ans == 1][:NUM_NUM]
ans_df_0 = all_data_df[all_data_df.ans == 0][:NUM_NUM]
train_df = pd.concat([ans_df_0, ans_df_1])

train_df = train_df.reindex(np.random.permutation(train_df.index)).reset_index(drop=True)

all_array = list(np.asarray(train_df.drop("ans", axis=1)))
all_answer = list(np.asarray(train_df.ans))

print("array made!!")

all_array_noimage = []
for i in range(len(all_answer)):
    array = all_array[i][133:1157]
    all_array_noimage.append(array)
    
all_array_notext = []
for i in range(len(all_answer)):
    array = all_array[i][1157:2058]
    all_array_notext.append(array)
    
all_array_base = []
for i in range(len(all_answer)):
    array = all_array[i][0:133]
    all_array_base.append(array)
    
all_array_baseimage = []
for i in range(len(all_answer)):
    array = all_array[i][0:1157]
    all_array_baseimage.append(array)
    
all_array_basetext = []
for i in range(len(all_answer)):
    array = all_array[i][0:1157]
    all_array_basetext.append(array)

print("grid search start!!")    
clf = RandomForestClassifier()

parameters = {
        'max_features'      : ["auto",None],
        'n_estimators'      : [10,100],
        'n_jobs'            : [40],
        'max_depth'         : [5,10,20,50],
        'criterion'         : ["entropy"]
}

# all
grid_search_1 = GridSearchCV(clf, param_grid=parameters)
start = time()
grid_search_1.fit(all_array, all_answer)
print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time() - start, len(grid_search_1.cv_results_['params'])))

gridsearch_all_1 = grid_search_1.cv_results_
best_estimator_1 = grid_search_1.best_estimator_
scores_1 = grid_search_1.best_score_ 
pickle.dump(best_estimator_1 ,open("/mnt/drobo/matsumoto/PICKLE_FILE_cost/gridsearch_"+str(NUM)+"_all.p","wb"))
print("number."+str(NUM)+"_all: ")

# image only
grid_search_1 = GridSearchCV(clf, param_grid=parameters)
start = time()
grid_search_1.fit(all_array_noimage, all_answer)
print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time() - start, len(grid_search_1.cv_results_['params'])))

gridsearch_all_1 = grid_search_1.cv_results_
best_estimator_1 = grid_search_1.best_estimator_
scores_1 = grid_search_1.best_score_ 
pickle.dump(best_estimator_1 ,open("/mnt/drobo/matsumoto/PICKLE_FILE_cost/gridsearch_"+str(NUM)+"_image.p","wb"))
print("number."+str(NUM)+"_image: ")

# text only
grid_search_1 = GridSearchCV(clf, param_grid=parameters)
start = time()
grid_search_1.fit(all_array_notext, all_answer)
print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time() - start, len(grid_search_1.cv_results_['params'])))

gridsearch_all_1 = grid_search_1.cv_results_
best_estimator_1 = grid_search_1.best_estimator_
scores_1 = grid_search_1.best_score_ 
pickle.dump(best_estimator_1 ,open("/mnt/drobo/matsumoto/PICKLE_FILE_cost/gridsearch_"+str(NUM)+"_text.p","wb"))
print("number."+str(NUM)+"_text: ")


# base only
grid_search_1 = GridSearchCV(clf, param_grid=parameters)
start = time()
grid_search_1.fit(all_array_base, all_answer)
print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time() - start, len(grid_search_1.cv_results_['params'])))

gridsearch_all_1 = grid_search_1.cv_results_
best_estimator_1 = grid_search_1.best_estimator_
scores_1 = grid_search_1.best_score_ 
pickle.dump(best_estimator_1 ,open("/mnt/drobo/matsumoto/PICKLE_FILE_cost/gridsearch_"+str(NUM)+"_base.p","wb"))
print("number."+str(NUM)+"_base: ")

# base image
grid_search_1 = GridSearchCV(clf, param_grid=parameters)
start = time()
grid_search_1.fit(all_array_baseimage, all_answer)
print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time() - start, len(grid_search_1.cv_results_['params'])))

gridsearch_all_1 = grid_search_1.cv_results_
best_estimator_1 = grid_search_1.best_estimator_
scores_1 = grid_search_1.best_score_ 
pickle.dump(best_estimator_1 ,open("/mnt/drobo/matsumoto/PICKLE_FILE_cost/gridsearch_"+str(NUM)+"_baseimage.p","wb"))
print("number."+str(NUM)+"_baseimage: ")

# base text
grid_search_1 = GridSearchCV(clf, param_grid=parameters)
start = time()
grid_search_1.fit(all_array_basetext, all_answer)
print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time() - start, len(grid_search_1.cv_results_['params'])))

gridsearch_all_1 = grid_search_1.cv_results_
best_estimator_1 = grid_search_1.best_estimator_
scores_1 = grid_search_1.best_score_ 
pickle.dump(best_estimator_1 ,open("/mnt/drobo/matsumoto/PICKLE_FILE_cost/gridsearch_"+str(NUM)+"_basetext.p","wb"))
print("number."+str(NUM)+"_basetext: ")


