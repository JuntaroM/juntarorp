# deleted titles that have 専用 in them, and changed date info to date and time it started selling. 
# -*- coding: utf-8 -*-

import time
from datetime import datetime, timedelta
import pymysql
import os
import glob
import chainer
import chainer.functions as F
from chainer.functions import caffe
from chainer import cuda
import os
import numpy as np
from PIL import Image
import pickle
import gensim
import MeCab
from vectorization import date_to_vector, category_to_vector, product_size_to_vector, product_condition_to_vector, delivery_charger_to_vector, shipping_method_to_vector, seller_address_to_vector, time_to_ship_to_vector

PATH = "/home/matsumoto/LOCAL_DIR/RANDOM_FOREST/VECTOR_PREPARATION/"
PATH_TO_IMAGE = "/mnt/hdd/matsumoto/MERCARI_IMAGES/"
PATH_TO_PICKLE_FILES = "/mnt/hdd/matsumoto/PICKLE_FILES_3/"
PATH_TO_CAFFE_FILES = "/home/matsumoto/LOCAL_DIR/RANDOM_FOREST/VECTOR_PREPARATION/CAFFE_FILES/"
PATH_TO_ENDOR_FILE = "/home/matsumoto/LOCAL_DIR/RANDOM_FOREST/VECTOR_PREPARATION/DATA_TRANSPORTATION/"
REQUIRED_TIME_FIRST_CLASS = 7200
DECLARATION = "START ALL OVER AGAIN"
#DECLARATION = "FROM LAST TIME"

print("[  NOTIFICATION  ] Start GPU settings")
# ----------------set GPU start----------------
gpu = 1
if gpu >= 0:
    cuda.check_cuda_available()
xp = cuda.cupy if gpu >= 0 else np
func = caffe.CaffeFunction(PATH_TO_CAFFE_FILES+'bvlc_googlenet.caffemodel')
if gpu >= 0:
    cuda.get_device(gpu).use()
    func.to_gpu()
os.environ['PATH'] += ':/usr/local/cuda-7.0/bin:/usr/local/cuda-7.0/bin'  #CUDAに通す

print("[  NOTIFICATION  ] Start getting data from MERCARI_DATA2")
# ----------------set TIME_START----------------
if "time.p" in os.listdir(PATH) and DECLARATION == "FROM LAST TIME":
    TIME_START = pickle.load( open( PATH + "time.p", "rb" ) )["TIME"]
else:
    TIME_START = '2016-12-28 18:30'
    
# ----------------connect to MySQL----------------
connector = pymysql.connect(host = '133.11.86.142', user = 'matsumoto', passwd = 'ud0nud0n', db = 'matsumoto', charset="utf8")
cursor = connector.cursor()

# ----------------getting the MERCARI_DATA2 list from mysql database----------------
statement = "SELECT id, date, required_time, sold_or_not, cost, category3, image_url, good_rating, normal_rating, bad_rating, number_of_products, product_size, product_condition, delivery_charger, shipping_method, seller_address, time_to_ship, title, brand_name, description FROM MERCARI_DATA2 WHERE date BETWEEN %s AND %s"
an_hour_ago = (datetime.today()-timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S')
cursor.execute(statement, (TIME_START,an_hour_ago))
mercari_data2_list = list(cursor.fetchall())
data2_list_count = len(mercari_data2_list)

# ----------------append endor's pickle file----------------
endor_data2_list = pickle.load( open(PATH_TO_ENDOR_FILE+"data_transport.p","rb") )
mercari_data2_list.extend(list(endor_data2_list))

# ----------------set TIME_START for next time----------------
time_pickle = {}
time_pickle["TIME"] = an_hour_ago
pickle.dump(time_pickle, open(PATH + "time.p","wb"), protocol=2)

# ----------------image to vector----------------
def image_to_vector(id2, PATH_TO_IMAGE, PATH_TO_CAFFE_FILES):   
        
    # ----------------set GPU----------------    
    # 入力画像サイズの定義
    image_shape = (224, 224)

    # 画像を読み込み、RGB形式に変換する
    image = Image.open(PATH_TO_IMAGE+id2+"_1.jpg").convert('RGB')
    
    # 画像のリサイズとクリップ
    image_w = image_shape[0]
    image_h = image_shape[1]
    w, h = image.size
    if w > h:
        shape = (int(image_w * w / h), image_h)
    else:
        shape = (image_w, int(image_h * h / w))
    x = (shape[0] - image_w) / 2
    y = (shape[1] - image_h) / 2
    image = image.resize(shape)
    image = image.crop((x, y, x + image_w, y + image_h))
    pixels = xp.asarray(image).astype(np.float32)
    
    # pixelsは3次元でそれぞれの軸は[Y座標, X座標, RGB]を表す
    # 入力データは4次元で[画像インデックス, BGR, Y座標, X座標]なので、配列の変換を行う
    # RGBからBGRに変換する
    pixels = pixels[:,:,::-1]

    # 軸を入れ替える
    pixels = pixels.transpose(2,0,1)

    # 平均画像を引く
    mean_image = xp.ndarray((3, 224, 224), dtype=np.float32)
    mean_image[0] = 103.939
    mean_image[1] = 116.779
    mean_image[2] = 123.68
    pixels -= mean_image

    # 4次元にする
    pixels = pixels.reshape((1,) + pixels.shape)

    # ----------------set GPU----------------
    # レイヤー'loss3/classifier'の出力を得て、softmaxを適用する
    x = chainer.Variable(pixels, volatile=True)
    y, = func(inputs={'data': x}, outputs=["pool5/7x7_s1"], train=False)
    prediction = F.softmax(y)
    
    #ラベルを読み込む
    categories = np.loadtxt(PATH_TO_CAFFE_FILES+'labels.txt', str, delimiter="\n")

    # スコアとラベルを紐づけスコアの高い順にソートする
    prediction_np = chainer.cuda.to_cpu(prediction.data.reshape((prediction.data.size,)))
    #result = zip(prediction_np, categories)
    #result = sorted(result, reverse=True)
    
    return list(prediction_np)

# ----------------separate Japanese sentences to words----------------
def mecab_ipadic_neologd_owakati(sentence_str):
    tagger_owakati = MeCab.Tagger("-Ochasen -d /usr/local/lib/mecab/dic/mecab-ipadic-neologd -Owakati")
    str_owakati = tagger_owakati.parse(sentence_str)
    return str_owakati.split(" ")

if __name__ == '__main__':
    
    # ----------------delete objects that have "専用" in title----------------
    delete_list = []
    for list_num in range(len(mercari_data2_list)):
        if "専用" in mercari_data2_list[list_num][3]:
            delete_list.append(list_num)
    count = 0
    for senyo_num in range(len(delete_list)):
        mercari_data2_list.pop(senyo_num-count)
        count += 1
    
    # ----------------vectorize the MERCARI_DATA2 data--title, brand_name, and description----------------
    title_list = []
    for i in range(data2_list_count):
        title_list.append(mercari_data2_list[i][17].replace("\n","").replace(" ","").replace("　",""))
    brand_name_list = []
    for i in range(data2_list_count):
        brand_name_list.append(mercari_data2_list[i][17].replace("\n","").replace(" ","").replace("　",""))
    description_list = []
    for i in range(data2_list_count):
        description_list.append(mercari_data2_list[i][17].replace("\n","").replace(" ","").replace("　",""))
        
    # ----------------training title to doc2vec----------------
    print("[  NOTIFICATION  ] Start training titles for MERCARI_DATA2 to doc2vec models")
    title_sentences = [mecab_ipadic_neologd_owakati(text_str.replace(" ", "")) for text_str in title_list]
    title_labeledSentences = gensim.models.doc2vec.LabeledListSentence(title_sentences)
    title_model = gensim.models.doc2vec.Doc2Vec(title_labeledSentences, min_count=0)
    # ----------------training brand_name to doc2vec----------------
    print("[  NOTIFICATION  ] Start training brand names for MERCARI_DATA2 to doc2vec models")
    brand_name_sentences = [mecab_ipadic_neologd_owakati(text_str.replace(" ", "")) for text_str in brand_name_list]
    brand_name_labeledSentences = gensim.models.doc2vec.LabeledListSentence(brand_name_sentences)
    brand_name_model = gensim.models.doc2vec.Doc2Vec(brand_name_labeledSentences, min_count=0)
    # ----------------training description to doc2vec----------------
    print("[  NOTIFICATION  ] Start training descriptions for MERCARI_DATA2 to doc2vec models")
    description_sentences = [mecab_ipadic_neologd_owakati(text_str.replace(" ", "")) for text_str in brand_name_list]
    description_labeledSentences = gensim.models.doc2vec.LabeledListSentence(brand_name_sentences)
    description_model = gensim.models.doc2vec.Doc2Vec(brand_name_labeledSentences, min_count=0)
    
    id_list = []
    label_and_vector = {}
    
    # ----------------vectorize the other MERCARI_DATA2 data, and bring them to pickle!!----------------
    for i in range(data2_list_count):
    
        # --------------------------------making vector pickle file!!!--------------------------------
        data2_vector = []
        # ----------------append variety of things to vector !!!----------------
        """
        # append date to vector
        data2_vector.extend(date_to_vector(mercari_data2_list[i][1]-timedelta(seconds=mercari_data2_list[i][2])))
        """
        # append date to vector
        data2_vector.extend(date_to_vector(mercari_data2_list[i][1]))
        # append cost to vector
        data2_vector.append(mercari_data2_list[i][4])
        # append category to vector
        data2_vector.extend(category_to_vector(mercari_data2_list[i][5]))
        # append number of images downloaded
        data2_vector.append(len(mercari_data2_list[i][6].split(", ")))
        # append good_rating to vector
        data2_vector.append(mercari_data2_list[i][7])
        # append normal_rating to vector
        data2_vector.append(mercari_data2_list[i][8])
        # append bad_rating to vector
        data2_vector.append(mercari_data2_list[i][9])
        # append number_of_products to vector
        data2_vector.append(mercari_data2_list[i][10])
        # append product_size to vector
        data2_vector.extend(product_size_to_vector(mercari_data2_list[i][11]))
        # append product_condition to vector
        data2_vector.extend(product_condition_to_vector(mercari_data2_list[i][12]))
        # append delivery_charger to vector
        data2_vector.extend(delivery_charger_to_vector(mercari_data2_list[i][13]))
        # append shipping_method to vector
        data2_vector.extend(shipping_method_to_vector(mercari_data2_list[i][14]))
        # append seller_address to vector
        data2_vector.extend(seller_address_to_vector(mercari_data2_list[i][15]))
        # append time_to_ship to vector
        data2_vector.extend(time_to_ship_to_vector(mercari_data2_list[i][16]))

        # ----------------append images to vector !!!----------------
        data2_vector.extend(image_to_vector(mercari_data2_list[i][0],PATH_TO_IMAGE,PATH_TO_CAFFE_FILES))
        
        # ----------------append title, brand_name, description to vector !!!----------------
        data2_vector.extend(list(title_model.syn0[i]))
        data2_vector.extend(list(brand_name_model.syn0[i]))
        data2_vector.extend(list(description_model.syn0[i]))

        label_and_vector[mercari_data2_list[i][0]+"_vector"] = data2_vector
        
        # --------------------------------making label pickle file!!!--------------------------------
        if mercari_data2_list[i][3] == "YES" and mercari_data2_list[i][2] <= REQUIRED_TIME_FIRST_CLASS:
            label = "FIRST CLASS"
        elif mercari_data2_list[i][3] == "YES" and mercari_data2_list[i][2] > REQUIRED_TIME_FIRST_CLASS:
            label = "SECOND CLASS"
        elif mercari_data2_list[i][3] == "NO":
            label = "THIRD CLASS"

        id_list.append(mercari_data2_list[i][0])
        label_and_vector[mercari_data2_list[i][0]+"_label"] = label

        print("[  SUCCESS  ] for "+str(i+1)+"th data installation in "+str(data2_list_count)+" datas")

    print("[  NOTIFICATIOIN  ] Start dumping all to pickle file")
    pickle.dump(label_and_vector, open(PATH_TO_PICKLE_FILES+'label_and_vector.p', mode='wb'), protocol=2)
    pickle.dump(id_list, open(PATH_TO_PICKLE_FILES+'id_list.p', mode='wb'), protocol=2)
    