# -*- coding: utf-8 -*-

# import datetime

CATEGORY_NUMBETR_LIST = ["category-314","category-316","category-317","category-318","category-319","category-321","category-322","category-323","category-324","category-326","category-327"]
PRODUCT_SIZE_LIST = ["XS以下","S","M","L","XL","XL以上","FREESIZE","no size"]
PRODUCT_CONDITION_LIST = ["新品、未使用","未使用に近い","目立った傷や汚れなし","やや傷や汚れあり","傷や汚れあり","全体的に状態が悪い"]
SHIPPING_METHOD_LIST = ["未定","らくらくメルカリ便","普通郵便(定形、定形外)","クロネコヤマト","ゆうパック","はこBOON","レターパック","クリックポスト","ゆうメール","ポスパケット/ゆうパケット","ポスパケット"]
SELLER_ADDRESS_LIST = ["東京都","大阪府","神奈川県","埼玉県","愛知県","千葉県","兵庫県","福岡県","北海道","京都府","静岡県","新潟県","岐阜県","岡山県","奈良県","茨城県","宮城県","滋賀県","広島県","熊本県","栃木県","群馬県","青森県","福島県","三重県","石川県","愛媛県","山口県","長野県","大分県","山形県","香川県","和歌山県","鹿児島県","岩手県","富山県","長崎県","沖縄県","山梨県","宮崎県","福井県","秋田県","鳥取県","高知県","徳島県","佐賀県","島根県"]


def date_to_vector(date_time):
    
    month = date_time.month
    day = date_time.day
    hour = date_time.hour
    minute = date_time.minute
    
    month_list = [0,0,0,0,0,0,0,0,0,0,0,0]
    for i in range(12):
        if month == i+1:
            month_list[i]=1
            
    day_of_the_month = [0,0,0]
    if day <= 10:
        day_of_the_month[0]=1
    elif day <=20 and day >10:
        day_of_the_month[1]=1
    else:
        day_of_the_month[2]=1
    
    hour_list = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    for i in range(24):
        if hour == i+1:
            hour_list[i]=1
    
    return month_list + day_of_the_month + hour_list

def category_to_vector(category3):
    
    category_vector = [0,0,0,0,0,0,0,0,0,0,0]
    
    for i in range(11):
        if category3 == CATEGORY_NUMBETR_LIST[i-1]:
            category_vector[i-1] = 1
    return category_vector

def product_size_to_vector(product_size):
    
    product_size_vector = [0,0,0,0,0,0,0,0]
    
    for i in range(8):
        if product_size == PRODUCT_SIZE_LIST[i-1]:
            product_size_vector[i-1] = 1
    return product_size_vector

def product_condition_to_vector(product_condition):
    
    product_condition_vector = [0,0,0,0,0,0]
    
    for i in range(6):
        if product_condition == PRODUCT_CONDITION_LIST[i-1]:
            product_condition_vector[i-1] = 1
            
    return product_condition_vector

def delivery_charger_to_vector(delivery_charger):
    if delivery_charger == "送料込み(出品者負担)":
        delivery_charger__list = [1,0]
    elif delivery_charger == "着払い(購入者負担)":
        delivery_charger__list = [0,1]
    else:
        delivery_charger__list = [0,0]
    return delivery_charger__list

def shipping_method_to_vector(shipping_method):
    
    shipping_method_vector = [0,0,0,0,0,0,0,0,0,0,0]
    
    for i in range(11):
        if shipping_method == SHIPPING_METHOD_LIST[i-1]:
            shipping_method_vector[i-1] = 1
    return shipping_method_vector

def seller_address_to_vector(seller_address):
    
    seller_address_vector = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    
    for i in range(47):
        if seller_address == SELLER_ADDRESS_LIST[i-1]:
            seller_address_vector[i-1] = 1
    return seller_address_vector

def time_to_ship_to_vector(time_to_ship):
    if time_to_ship == "1~2日で発送":
        time_to_ship_list = [1,0,0]
    elif time_to_ship == "2~3日で発送":
        time_to_ship_list = [0,1,0]
    elif time_to_ship == "4~7日で発送":
        time_to_ship_list = [0,0,1]
    else:
        time_to_ship_list = [0,0,0]
    return time_to_ship_list
