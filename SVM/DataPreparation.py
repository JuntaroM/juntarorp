# -*- coding: utf-8 -*-

import os
import cv
import cv2
import numpy as np

#function for making a directory
def mkdir(dirname):
    try:
        os.mkdir(dirname)
    except Exception as e:
        print e, "error occured at making directory"


#(going to be) the function for converting 3 files to training set and testing set(resized).
def DataPreparation(PATH_TO_DIRECTORY,NAME_OF_CHARACTER,DIRECTORY_NAME):

    #for training data
    mkdir(DIRECTORY_NAME[0])
    mkdir(DIRECTORY_NAME[1])

    success_num = [0,0,0]
    image_num = [0,0,0]
    for name_of_character in range(3):
        #getting all the images from file
        all_images = os.listdir(PATH_TO_DIRECTORY+"/"+NAME_OF_CHARACTER[name_of_character]+"File")
        image_num[name_of_character] = len(all_images)
        
        #resizing and saving each image in all_images
        for image in all_images:
            im = cv2.imread(PATH_TO_DIRECTORY+"/"+NAME_OF_CHARACTER[name_of_character]+"File"+"/"+image)
            
            #resizing data
            try:
                resized_image = cv2.resize(im,(50,50))
        
                # 4 training data, for 1 test data
                if success_num[name_of_character] % 5 == 0:
                    dirname = DIRECTORY_NAME[1]
                else:
                    dirname = DIRECTORY_NAME[0]
                
                #saving data
                try:
                    cv2.imwrite(os.path.join(dirname,NAME_OF_CHARACTER[name_of_character] + str(success_num[name_of_character])+".jpg"),resized_image)
                    print "[ Success ]" + image
                    success_num[name_of_character] += 1
                except Exception as e:
                    print e, "error occured at saving" + image

            except Exception as e:
                print e, "error occured at resizing", image
                    
    print sum(success_num), "images could be resized (in", sum(image_num), "images)."



