# -*- coding: utf-8 -*-

from DataPreparation import DataPreparation
from Labelling import Labelling
import os
import cv2
import numpy as np
from sklearn import svm
from sklearn.metrics import classification_report, accuracy_score

PATH_TO_DIRECTORY = "/Users/JT/Programming/python/sklearn-svm"
NAME_OF_CHARACTER = ["Stitch","Jasmine","Pluto"]
DIRECTORY_NAME = ["TrainingData","TestingData"]


if __name__ == '__main__':
    
    #Preparing data from Stitch, Jasmine, and Pluto Files
    try:
        DataPreparation(PATH_TO_DIRECTORY,NAME_OF_CHARACTER,DIRECTORY_NAME)
    except Exception as e:
        print e, "error occured at Data Preparation"

    #labeling the training data and test data
    try:
        train_labels = Labelling(PATH_TO_DIRECTORY,NAME_OF_CHARACTER,DIRECTORY_NAME,0)
        test_labels = Labelling(PATH_TO_DIRECTORY,NAME_OF_CHARACTER,DIRECTORY_NAME,1)
    except Exception as e:
        print e, "error occured at labelling"

    #making train_features, train_labels
    all_images_tr = os.listdir(PATH_TO_DIRECTORY+"/"+DIRECTORY_NAME[0])

    train_features = []
    for image in all_images_tr:
        each_image1 = cv2.imread(PATH_TO_DIRECTORY+"/"+DIRECTORY_NAME[0]+"/"+image)
        each_image2 = np.reshape(each_image1,(1,np.product(each_image1.shape)))
        each_image3 = list(each_image2[0])
        train_features.append(each_image3)

    #svm start!!
    #svm to training data!
    try:
        clf = svm.SVC()
        clf.fit(train_features, train_labels)
    except Exception as e:
        print e, "error occured at training process"

    #preparing test_features
    all_images_tst = os.listdir(PATH_TO_DIRECTORY+"/"+DIRECTORY_NAME[1])
    test_features = []
    for image in all_images_tst:
        each_image1 = cv2.imread(PATH_TO_DIRECTORY+"/"+DIRECTORY_NAME[1]+"/"+image)
        each_image2 = np.reshape(each_image1,(1,np.product(each_image1.shape)))
        each_image3 = list(each_image2[0])
        test_features.append(each_image3)

    #svm to test data!
    test_pred = clf.predict(test_features)

    #svm results
    print classification_report(test_labels, test_pred)
    print "Accuracy for image recognition was", accuracy_score(test_labels, test_pred)



