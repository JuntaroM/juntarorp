# -*- coding: utf-8 -*-

import urllib2
import urllib
import os
from bs4 import BeautifulSoup

URL_stitch = "http://disney.wikia.com/wiki/Stitch/Gallery/Animation"
URL_jasmine = "http://disney.wikia.com/wiki/Jasmine/Gallery/Films_and_Television"
URL_pluto = "http://disney.wikia.com/wiki/Pluto/Gallery/Animation"
URL = [URL_stitch,URL_jasmine,URL_pluto]
GALLERYNUMBER = [7,9,14]
FILENAME = ["StitchFile","JasmineFile","PlutoFile"]

#function for making a directory
def mkdir(dirname):
    try:
        os.mkdir(dirname)
    except Exeption as e:
        print(e)

#function for changing the source to the URL
def get_img_url(x,url):
    if "http" not in x:
        return url+x
    else:
        return x

#main function: downloading the images
def dl_images(image_items,dirname):
    success_num = 0
    for img in image_items:
        source = img.get("data-src")
        img_url = get_img_url(source, URL)
        fname = source.split("/")[-1]

        try:
            if fname in os.listdir(dirname):
                fname = fname + str(success_num)
            urllib.urlretrieve(img_url, dirname+"/"+fname+".jpg")
            print "[ Success ]" + img_url
            success_num += 1
        except Exception, e:
                print e, "error occured for" + img_url
    return success_num


if __name__ == '__main__':
    
    #initial value determination
    success_num = [0,0,0]
    image_num = [0,0,0]
    
    for character_num in range(3):

        mkdir(FILENAME[character_num])

        #using beautiful soup
        html = urllib2.urlopen(URL[character_num])
        soup = BeautifulSoup(html, "html.parser")
        
        #getting all the images from html: 7 gallerys
        count = 0
        image_items = []
        for gal_num in range(GALLERYNUMBER[character_num]):
            images_part = soup.find("div", {"id":"gallery-"+str(gal_num)})
            image_items_1 = images_part.find_all('img')
            image_items_2 = filter(lambda x: x.get('data-src'), image_items_1)
            image_items.extend(image_items_2)
            count += 1

        #main!!
        success_num[character_num] = dl_images(image_items,FILENAME[character_num])
        image_num[character_num] = len(image_items)

    print sum(success_num), "images could be downloaded (in ",sum(image_num)," images)."
