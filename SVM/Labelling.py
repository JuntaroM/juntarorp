# -*- coding: utf-8 -*-

import os

def Labelling(PATH_TO_DIRECTORY,NAME_OF_CHARACTER,DIRECTORY_NAME,DIRECTORY_NUMBER):

    #making a list for training data
    all_images_tr = os.listdir(PATH_TO_DIRECTORY+"/"+DIRECTORY_NAME[DIRECTORY_NUMBER])

    #label list
    Labelling = []
    for image in all_images_tr:
        if NAME_OF_CHARACTER[0] in image:
            Labelling.append(NAME_OF_CHARACTER[0])
        elif NAME_OF_CHARACTER[1] in image:
            Labelling.append(NAME_OF_CHARACTER[1])
        elif NAME_OF_CHARACTER[2] in image:
            Labelling.append(NAME_OF_CHARACTER[2])
        else:
            print "error occured at labelling" + image

    return Labelling
