# -*- coding: utf-8 -*-

import numpy as np
import cv2
from sklearn.cluster import KMeans

IMAGE_PATH = "/Users/JT/Programming/python/WebScraping/kmeans-clustering/stitch_data"

if __name__ == '__main__':
    
    all_images = []
    for image_num in range(1,608):
        each_image1 = cv2.imread(IMAGE_PATH+"/stitch"+str(image_num)+".jpg")
        each_image2 = np.reshape(each_image1, (1,np.product(each_image1.shape)))
        each_image3 = list(each_image2[0])
        all_images.append(each_image3)

    pred = KMeans(n_clusters=4).fit_predict(all_images)

    print pred

