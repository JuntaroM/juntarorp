# -*- coding: utf-8 -*-

import urllib2
import urllib
import re
import time
import datetime
import os
from bs4 import BeautifulSoup

URL = "http://disney.wikia.com/wiki/Stitch/Gallery/Animation"

#function for making a directory
def mkdir(dirname):
    try:
        os.mkdir(dirname)
    except Exeption as e:
        print(e)

#function for changing the source to the URL
def get_img_url(x,url):
    if "http" not in x:
        return url+x
    else:
        return x

#function for downloading image
def dl_images(image_items):
    success_num = 0
    for img in image_items:
        source = img.get("data-src")
        img_url = get_img_url(source, URL)
        fname = source.split("/")[-1]

        try:
            if fname in os.listdir(dirname):
                fname = fname + str(success_num)
            urllib.urlretrieve(img_url, dirname+"/"+fname+".jpg")
            print "[ Success ]" + img_url
            success_num += 1
        except Exception, e:
                print e, "error occured for" + img_url
    return success_num


if __name__ == '__main__':
    
    #deciding dirname
    now = datetime.datetime.today()
    dirname = '{}{:0>2}{:0>2}{:0>2}{:0>2}{:0>6}'\
        .format(now.year, now.month, now.day,
                now.minute, now.second, now.microsecond)
    mkdir(dirname)

    #applying beautifulsoup
    html = urllib2.urlopen(URL)
    soup = BeautifulSoup(html, "html.parser")

    #getting the part in html where stitch is at
    count = 0
    image_items = []
    for gal_num in range(7):
        images_part = soup.find("div", {"id":"gallery-"+str(gal_num)})
        image_items_1 = images_part.find_all('img')
        image_items_2 = filter(lambda x: x.get('data-src'), image_items_1)
        image_items.extend(image_items_2)
        count += 1

    #main part!!
    success_num = dl_images(image_items)
    image_num = len(image_items)
    print success_num, "images could be downloaded (in ",image_num," images)."
