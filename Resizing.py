# -*- coding: utf-8 -*-

import os
import cv
import cv2
import numpy as numpy
import datetime

PATH_TO_IMAGE = "/Users/JT/Programming/python/WebScraping/201610164530956160"
NAME_OF_CHARACTER = "stitch"

#function for making a directory
def mkdir(dirname):
    try:
        os.mkdir(dirname)
    except Exception as e:
        print e, "error occured at making directory"

#function for resizing the image(which is already cv2.imread done)
def resizing(im):
    try:
        gazou = cv2.resize(im,(50,50))
        return gazou
    except Exception as e:
        print e, "error occured at resizing"

#function for saving the image to a folder "dirname" as "write_image_name"
def image_save(dirname,write_image_name,resized_image):
    try:
        cv2.imwrite(os.path.join(dirname,write_image_name),resized_image)
    
    except Exception as e:
        print e, "error occured at saving"

#main function: getting each image in all_images, resizing, and saving them.
def main(dirname,all_images):
    success_num = 0
    for image in all_images:
        im = cv2.imread(PATH_TO_IMAGE+"/"+image)
        resized_image = resizing(im)
        try:
            image_save(dirname,NAME_OF_CHARACTER + str(success_num)+".jpg",resized_image)
            print "[ Success ]" + image
            success_num += 1
        except Exception as e:
            print e, "error occured for" + image
    return success_num

if __name__ == '__main__':
    
    #deciding the dirname
    now = datetime.datetime.today()
    dirname = '{}{:0>2}{:0>2}{:0>2}{:0>2}{:0>6}'\
        .format(now.year, now.month, now.day,
                now.minute, now.second, now.microsecond)
    mkdir(dirname)

    #getting all the images from file
    all_images = os.listdir(PATH_TO_IMAGE)

    #main function!!!
    success_num = main(dirname,all_images)

    image_num = len(all_images)
    print success_num, "images could be resized (in", image_num, "images)."
